<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Puntaje extends Model
{
    protected $fillable = [
        'nombre_usuario', 'nombre_real', 'puntaje'
    ];
    
}
