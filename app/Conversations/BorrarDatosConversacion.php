<?php

namespace App\Conversations;

use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;

class BorrarDatosConversacion extends Conversation
{
    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run()
        {
	    $this->confirmar();
        }

    private function confirmar()
    {
        $consulta = Question::create("¿Estás seguro de borrar toda tu información?")
        ->addButtons([
            Button::create("Si")->value('si'),
            Button::create("No")->value('no')
    ]);

        $this->ask($consulta, function (Answer $answer)
        {
            $valor = $answer->getValue();

            switch($valor)
            {
                case "si":
                    $this->borrarDatos();
                    $this->say("He borrado toda tu información ... ¿Quién eres?.");
                break;

                default:
                    $this->say("Ok, no he borrado nada.");
                break;
            }
        });
    }

    private function borrarDatos()
    {
        $nombre_usuario = $this->bot->getUser()->getUsername() ?: "desconocido";

        return \App\Puntaje::where('nombre_usuario',
                    $nombre_usuario)->delete();
    }

}
