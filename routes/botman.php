<?php
use App\Http\Controllers\BotManController;

$botman = resolve('botman');

$botman->hears('/start', function ($bot) {
	$nombres = $bot->getUser()->getFirstName() ?: "desconocido";
	$bot->reply('Hola ' . $nombres . ', bienvenido al bot SimpleQuizzes!');
});

$botman->hears('Hi', function ($bot) {
    $bot->reply('Hello!');
});

$botman->hears('/ayuda', function ($bot) {
	$ayuda = ['/ayuda' => 'Mostrar este mensaje de ayuda',
          	'acerca de|acerca' => 'Ver la información quien desarrollo este lindo bot',
          	'listar quizzes|listar' => 'Listar los cuestionarios disponibles',
          	'iniciar quiz <id>' => 'Iniciar la solución de un cuestionario',
          	'ver puntajes|puntajes' => 'Ver los últimos puntajes',
          	'borrar mis datos|borrar' => 'Borrar mis datos del sistema'];
    
	$bot->reply("Los comandos disponibles son:");

	foreach($ayuda as $key=>$value)
	{
    		$bot->reply($key . ": " . $value);
	}
});

$botman->hears('Acerca de|Acerca', function ($bot) {
    $bot->reply("Este Bot de charla fue desarrollado por:\n".
                "Diego E. Hurtado <diego.hurtadoq@autonoma.edu.co>\n".
                "durante el curso de Procesos Ágiles de SW - 2019");
});

$botman->hears('listar quizzes|listar', function ($bot) {
	$quizzes = \App\Quiz::orderby('titulo', 'asc')->get();

	foreach($quizzes as $quiz)
	{
    		$bot->reply($quiz->id."- ".$quiz->titulo);
	}

	if(count($quizzes) == 0)
    		$bot->reply("Ups, no hay cuestionarios para mostrar.");
});

$botman->hears('iniciar quiz {id}', function ($bot, $id) {
	$bot->startConversation(
new \App\Conversations\RealizarQuizConversacion($id));
})->stopsConversation();

$botman->hears('ver puntajes|puntajes', function ($bot) {
	$puntajes = \App\Puntaje::orderby('created_at', 'desc')->limit(10)->get();

	foreach($puntajes as $puntaje)
	{
    	$fecha = (new \DateTime($puntaje->created_at))
->format("d/m/Y");
    	$hora = (new \DateTime($puntaje->created_at))
->format("H:i");

    	$msj = "$puntaje->nombre_real ($puntaje->nombre_usuario) obtuvo ".
           	"$puntaje->puntaje puntos en $fecha a las $hora";

    	$bot->reply($msj);
	}
});

$botman->hears('borrar mis datos|borrar', function ($bot) {
	$bot->startConversation(
new \App\Http\Conversations\BorrarDatosConversacion());
})->stopsConversation();



//$botman->hears('Start conversation', BotManController::class.'@startConversation');

$botman->fallback(function ($bot) {
	$bot->reply("No entiendo que quieres decir, vuelve a intentarlo.");
});